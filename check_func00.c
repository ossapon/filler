/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_func00.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 22:14:23 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:33:59 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			check_side(t_player *player)
{
	int		y;

	y = player->map.height;
	while (--y >= 0)
		if (player->map.map[y][player->map.width - 1] == player->my_fig
			|| player->map.map[y][0] == player->my_fig)
			return (1);
	return (0);
}

int			def_check(t_player *player)
{
	int		y;

	y = -1;
	if (ft_strchr(player->map.map[0], player->my_fig))
		return (0);
	while (++y < player->map.height)
	{
		if (ft_strchr(player->map.map[y], player->my_fig)
			&& ft_strchr(player->map.map[y], player->enemy_fig))
			return (1);
	}
	return (0);
}

int			check_attack_coord(t_player *player)
{
	if (player->map.my_x < player->map.enemy_x)
		return (1);
	if (player->map.my_x >= player->map.enemy_x)
		return (1);
	return (0);
}

int			check_pos_x(t_player *player, int *y)
{
	int		x;

	x = -1;
	while (++x < player->map.width)
	{
		if (player->map.my_x >= player->map.enemy_x)
		{
			if (*y + 1 < player->map.height
				&& (ft_strchr(player->map.map[*y + 1], player->my_fig)
					|| (ft_strchr(player->map.map[*y + 1], player->enemy_fig)
						&& ft_strchr(player->map.map[*y], player->my_fig))))
				return (x);
		}
		if (player->map.my_x < player->map.enemy_x)
			if (*y + 1 < player->map.height
				&& player->map.map[*y][x] == player->my_fig
				&& player->map.map[*y][x + 1] == '.'
				&& player->map.map[*y + 1][x + 1] == '.'
				&& player->map.map[*y + 1][x] == '.')
				return (x);
	}
	return (x);
}
