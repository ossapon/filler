/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 22:12:14 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:38:06 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int				check_need_cood_vs_x_pos(t_player *player, int y, int x)
{
	if (player->map.my_x < player->map.enemy_x)
		if ((player->map.map[y][x] == '.')
			|| (player->map.map[y][x] == player->my_fig
				&& player->map.map[y][x + 1] != player->my_fig))
			return (1);
	if (player->map.my_x >= player->map.enemy_x)
		if ((player->map.map[y][x] == '.' && player->map.map[y][x - 1] == '.')
			|| (player->map.map[y][x] == player->my_fig
				&& player->map.map[y][x - 1] != player->my_fig)
			|| (y - 1 >= 0 && player->map.map[y][x] == player->my_fig
				&& player->map.map[y - 1][x] == '.'))
			return (1);
	return (0);
}

void			analize_board(t_player *player)
{
	if (player->map.my_y < player->map.enemy_y)
		attack_down(player);
	else if (player->map.my_y >= player->map.enemy_y
			&& !ft_strchr(player->map.map[0], player->my_fig))
		attack_up(player);
	if (def_check(player))
		defence(player);
	check_the_field(player);
}

int				main(void)
{
	char		*line;
	t_player	player;

	player.queue = 0;
	if (player.queue == 0 && get_next_line(0, &line) > 0)
	{
		my_queue(line, &player);
		ft_strdel(&line);
	}
	while (1)
	{
		get_info(&player);
		analize_board(&player);
		free_map_and_piece(&player);
		ft_printf("%d %d\n", player.y_coord, player.x_coord);
		if (player.x_coord == 0 && player.y_coord == 0)
			break ;
	}
	return (0);
}
