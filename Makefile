# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/15 18:50:54 by osapon            #+#    #+#              #
#    Updated: 2018/03/27 22:47:43 by osapon           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = osapon.filler

FLAGS = -Wall -Wextra -Werror -o

SRCS = filler.c get_info.c get_map_piece.c ad_func.c check_func01.c main.c \
       check_func00.c

OBJ=$(SRCS:.c=.o)

all: $(NAME)
	    
$(NAME) : $(OBJ) 
		make -C libft/
		gcc $(FLAGS) $(NAME) $(SRCS)  -I /bin/ -L./libft -lft
clean:
		make -C libft/ clean
		/bin/rm -f $(OBJ)
fclean: clean
		rm -f libft/libft.a
		/bin/rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re norm
		.NOTPARALLEL: all clean fclean re norm

norm:
		norminette *.c* .h
