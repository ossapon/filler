/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ad_func.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 12:09:12 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:32:13 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		get_my_xy(t_player *player, char *line, int *i)
{
	int		x;

	player->map.my_y = *i;
	line = ft_strchr(line, ' ') + 1;
	x = -1;
	while (line[++x])
		if (line[x] == player->my_fig)
			player->map.my_x = x;
}

void		get_enemy_xy(t_player *player, char *line, int *i)
{
	int		x;

	player->map.enemy_y = *i;
	line = ft_strchr(line, ' ') + 1;
	x = -1;
	while (line[++x])
		if (line[x] == player->enemy_fig)
			player->map.enemy_x = x;
}

void		free_map_and_piece(t_player *player)
{
	int		i;

	i = -1;
	while (++i < player->map.height)
		ft_strdel(&(player->map.map[i]));
	i = -1;
	while (++i < player->piece.height)
		ft_strdel(&(player->piece.piece[i]));
}

static void	piece_coord(t_player *player)
{
	int		i;
	int		x;
	int		y;

	i = 0;
	y = -1;
	while (++y < player->piece.height)
	{
		x = -1;
		while (++x < player->piece.width)
			if (player->piece.piece[y][x] == '*')
			{
				player->piece.coord[i].y = y;
				player->piece.coord[i].x = x;
				i++;
			}
	}
}

void		count_piece(t_player *player)
{
	int		x;
	int		y;

	player->piece.count = 0;
	y = -1;
	while (++y < player->piece.height)
	{
		x = -1;
		while (++x < player->piece.width)
			if (player->piece.piece[y][x] == '*')
				player->piece.count++;
	}
	player->piece.coord = (t_coord*)malloc(sizeof(t_coord) *
												(player->piece.count));
	piece_coord(player);
}
