/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 13:02:00 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:40:41 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		attack_down(t_player *player)
{
	int		y;
	int		x;

	y = check_pos_y(player);
	while (y < player->map.height)
	{
		x = check_pos_x(player, &y);
		while (x < player->map.width)
		{
			if (y + 1 < player->map.height && x < player->map.width
			&& check_attack_coord(player)
			&& check_valid_space(player, y - player->piece.coord[0].y,
		x - player->piece.coord[0].x))
			{
				player->y_coord = y + 0 - player->piece.coord[0].y;
				player->x_coord = x + 0 - player->piece.coord[0].x;
				return ;
			}
			x++;
		}
		y++;
	}
}

void		defence(t_player *player)
{
	int		x;
	int		y;

	y = player->map.height;
	while (--y > 0)
	{
		x = player->map.width;
		while (--x > 0)
		{
			if (y + 1 < player->map.height && check_valid_space(player,
						y - player->piece.coord[player->piece.count - 1].y,
						x - player->piece.coord[player->piece.count - 1].x)
			&& ((!ft_strchr(player->map.map[y], player->enemy_fig)
			&& ft_strchr(player->map.map[y + 1], player->enemy_fig))
			|| !ft_strchr(player->map.map[y - 1], player->my_fig))
			&& check_need_cood_vs_x_pos(player, y, x))
			{
				player->y_coord = y + 0 - player->piece.coord
										[player->piece.count - 1].y;
				player->x_coord = x + 0 - player->piece.coord
										[player->piece.count - 1].x;
				return ;
			}
		}
	}
}

void		attack_other_side(t_player *player)
{
	int		y;
	int		x;

	y = (player->map.my_y > player->map.enemy_y) ? 0 : player->map.height - 1;
	while (y >= 0 && y < player->map.height)
	{
		x = (player->map.my_x > player->map.enemy_x) ? 0
													: player->map.width - 1;
		while (x >= 0 && x < player->map.width)
		{
			if (y + 1 <= player->map.height && x >= 0
				&& check_valid_space(player,
				y - player->piece.coord[player->piece.count - 1].y,
				x - player->piece.coord[player->piece.count - 1].x))
			{
				player->y_coord = y + 0 - player->piece.coord
										[player->piece.count - 1].y;
				player->x_coord = x + 0 - player->piece.coord
										[player->piece.count - 1].x;
				return ;
			}
			x = (player->map.my_x > player->map.enemy_x) ? x + 1 : x - 1;
		}
		y = (player->map.my_y < player->map.enemy_y) ? y + 1 : y - 1;
	}
}

void		fill_the_board(t_player *player)
{
	int		y;
	int		x;

	y = player->map.height;
	while (--y >= 0)
	{
		x = player->map.width;
		while (--x >= 0)
		{
			if (check_valid_space(player, y - player->piece.coord[0].y,
								x - player->piece.coord[0].x))
			{
				player->y_coord = y + 0 - player->piece.coord[0].y;
				player->x_coord = x + 0 - player->piece.coord[0].x;
				return ;
			}
		}
	}
}

void		attack_up(t_player *player)
{
	int		x;
	int		y;

	y = -1;
	while (++y < player->map.height)
	{
		x = (player->map.my_x > player->map.enemy_x) ? 0
													: player->map.width - 1;
		while (x >= 0 && x < player->map.width)
		{
			if (x + 1 < player->map.width && y < player->map.height
				&& check_need_cood_vs_x_pos(player, y, x)
				&& check_valid_space(player,
				y - need_coord_y(player),
				x - need_coord_x(player)))
			{
				player->y_coord = y + 0 - need_coord_y(player);
				player->x_coord = x + 0 - need_coord_x(player);
				return ;
			}
			x = (player->map.my_x > player->map.enemy_x) ? x + 1 : x - 1;
		}
	}
}
