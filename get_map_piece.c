/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_piece.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/18 13:03:36 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:37:26 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		my_queue(char *line, t_player *player)
{
	line = ft_strchr(line, 'p');
	if (line[1] == '1' && ft_strstr(line, "osapon"))
	{
		player->queue = FST;
		player->my_fig = 'O';
		player->enemy_fig = 'X';
	}
	else
	{
		player->queue = SEC;
		player->my_fig = 'X';
		player->enemy_fig = 'O';
	}
}

void		wh_map(t_player *player, char *line)
{
	player->map.height = 0;
	player->map.width = 0;
	line = ft_strchr(line, ' ') + 1;
	player->map.height = ft_atoi(line);
	line = ft_strchr(line, ' ');
	player->map.width = ft_atoi(line);
}

void		malloc_map(t_player *player)
{
	int		i;

	player->map.map = (char**)malloc(sizeof(char*) * (player->map.height) + 1);
	(player->map.map)[player->map.height] = NULL;
	i = 0;
	while (i < player->map.height)
		(player->map.map)[i++] = ft_memalloc((size_t)player->map.width + 1);
}

void		malloc_piece(t_player *player)
{
	int		i;

	player->piece.piece = (char**)malloc(sizeof(char*)
										* (player->piece.height + 1));
	(player->piece.piece)[player->piece.height] = NULL;
	i = 0;
	while (i < player->piece.height)
		(player->piece.piece)[i++] =
				ft_memalloc(((size_t)player->piece.width + 1));
}

void		piece_create(t_player *player, char *line)
{
	player->piece.height = 0;
	player->piece.width = 0;
	line = ft_strchr(line, ' ') + 1;
	player->piece.height = ft_atoi(line);
	line = ft_strchr(line, ' ') + 1;
	player->piece.width = ft_atoi(line);
	malloc_piece(player);
}
