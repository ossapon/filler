/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_info.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/18 12:55:38 by osapon            #+#    #+#             */
/*   Updated: 2018/03/24 11:46:53 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		check_the_field(t_player *player)
{
	if (check_side(player) || ft_strchr(player->map.map[0], player->my_fig))
		attack_other_side(player);
	if (!player->y_coord && !player->x_coord)
		fill_the_board(player);
}

void		get_piece(t_player *player)
{
	char	*line;
	int		i;

	i = -1;
	while (++i < player->piece.height)
	{
		get_next_line(0, &line);
		(player->piece.piece)[i] = ft_strcpy((player->piece.piece)[i], line);
		ft_strdel(&line);
	}
}

void		fill_map(t_player *player, int *i)
{
	char	*line;

	while (get_next_line(0, &line) > 0)
	{
		if (ft_strstr(line, "Piece"))
		{
			piece_create(player, line);
			ft_strdel(&line);
			get_piece(player);
			count_piece(player);
			break ;
		}
		if (!player->map.enemy_y && !player->map.enemy_x
			&& ft_strchr(line, player->enemy_fig))
			get_enemy_xy(player, line, i);
		if (!player->map.my_y && !player->map.my_x
			&& ft_strchr(line, player->my_fig))
			get_my_xy(player, line, i);
		(player->map.map)[(*i)] = ft_strcpy((player->map.map)[(*i)],
													ft_strchr(line, ' ') + 1);
		ft_strdel(&line);
		(*i)++;
	}
}

void		get_map(t_player *player)
{
	char	*line;
	int		i;

	if (get_next_line(0, &line) > 0)
		wh_map(player, line);
	ft_strdel(&line);
	get_next_line(0, &line);
	if (player->map.width > 0 && player->map.height > 0)
		malloc_map(player);
	ft_strdel(&line);
	i = 0;
	fill_map(player, &i);
}

void		get_info(t_player *player)
{
	player->map.my_x = 0;
	player->map.my_y = 0;
	player->map.enemy_x = 0;
	player->map.enemy_y = 0;
	get_map(player);
	player->x_coord = 0;
	player->y_coord = 0;
}
