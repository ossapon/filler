/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_func01.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/27 22:26:04 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:44:13 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			need_coord_x(t_player *player)
{
	int		y;
	int		x;
	int		i;

	i = 0;
	y = -1;
	while (++y < player->piece.height)
	{
		x = player->piece.width;
		if (y + 1 < player->piece.height)
			while (--x >= 0)
				if (player->piece.piece[y][x] == '*')
				{
					i = player->piece.count - 1;
					break ;
				}
	}
	return (player->piece.coord[i].x);
}

int			need_coord_y(t_player *player)
{
	int		y;
	int		x;
	int		i;

	i = 0;
	y = -1;
	while (++y < player->piece.height)
	{
		x = -1;
		if (y + 1 < player->piece.height)
			while (++x < player->piece.width)
				if (player->piece.piece[y][x] == '*')
				{
					i++;
					break ;
				}
	}
	return (player->piece.coord[i].y);
}

int			check_pos_y(t_player *player)
{
	int		y;
	int		pos;

	y = -1;
	pos = 0;
	while (++y < player->map.height)
	{
		if (y - 1 >= 0 && ft_strchr(player->map.map[y], player->my_fig)
			&& ft_strchr(player->map.map[y - 1], player->enemy_fig)
			&& player->queue == SEC)
			pos = 0 + y;
		if (y + 1 < player->map.height
			&& ft_strchr(player->map.map[y], player->my_fig)
			&& !ft_strchr(player->map.map[y + 1], player->enemy_fig)
			&& player->queue == FST)
			pos = 0 + y;
	}
	return (pos);
}

int			check_my_piece(t_player *player, int y, int x, int i)
{
	if (y + player->piece.coord[i].y < player->map.height
		&& x + player->piece.coord[i].x < player->map.width
		&& x + player->piece.coord[i].x >= 0
		&& y + player->piece.coord[i].y >= 0
		&& player->map.map[y + player->piece.coord[i].y]
		[x + player->piece.coord[i].x] == player->my_fig)
		return (1);
	return (0);
}

int			check_valid_space(t_player *player, int y, int x)
{
	int		i;
	int		success;

	i = -1;
	success = 0;
	while (++i < player->piece.count)
		if (check_my_piece(player, y, x, i))
			success++;
	if (success != 1)
		return (0);
	i = -1;
	while (++i < player->piece.count)
	{
		if (y + player->piece.coord[i].y < player->map.height
			&& x + player->piece.coord[i].x < player->map.width
			&& x + player->piece.coord[i].x >= 0
			&& y + player->piece.coord[i].y >= 0
			&& player->map.map[y + player->piece.coord[i].y]
			[x + player->piece.coord[i].x] == '.')
			success++;
	}
	return ((success == player->piece.count) ? 1 : 0);
}
