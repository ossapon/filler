/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 13:02:00 by osapon            #+#    #+#             */
/*   Updated: 2018/03/27 22:29:26 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# define FST 1
# define SEC 2

# include "libft/libft.h"
# include "libft/get_next_line.h"
# include "libft/ft_printf.h"

typedef struct	s_coord
{
	int			x;
	int			y;
}				t_coord;

typedef struct	s_piece
{
	int			width;
	int			height;
	int			count;
	char		**piece;
	t_coord		*coord;
}				t_piece;

typedef struct	s_map
{
	int			width;
	int			height;
	int			my_x;
	int			my_y;
	int			enemy_x;
	int			enemy_y;
	char		**map;
}				t_map;

typedef struct	s_player
{
	int			queue;
	char		my_fig;
	char		enemy_fig;
	int			x_coord;
	int			y_coord;
	t_map		map;
	t_piece		piece;
}				t_player;

void			get_info(t_player *player);
void			my_queue(char *line, t_player *player);
void			wh_map(t_player *player, char *line);
void			malloc_map(t_player *player);
void			malloc_piece(t_player *player);
void			piece_create(t_player *player, char *line);
void			free_map_and_piece(t_player *player);
void			count_piece(t_player *player);
void			get_enemy_xy(t_player *player, char *line, int *i);
void			get_my_xy(t_player *player, char *line, int *i);
int				check_need_cood_vs_x_pos(t_player *player, int y, int x);
void			analize_board(t_player *player);
int				check_side(t_player *player);
int				def_check(t_player *player);
int				check_attack_coord(t_player *player);
int				check_pos_x(t_player *player, int *y);
void			attack_up(t_player *player);
void			check_the_field(t_player *player);
void			defence(t_player *player);
void			attack_down(t_player *player);
void			fill_the_board(t_player *player);
void			attack_other_side(t_player *player);
int				check_valid_space(t_player *player, int y, int x);
int				need_coord_x(t_player *player);
int				need_coord_y(t_player *player);
int				check_pos_y(t_player *player);

#endif
